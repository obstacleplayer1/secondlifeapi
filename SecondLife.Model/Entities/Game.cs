﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SecondLifeModel.Entities
{
    /// <summary>
    /// Représente tous les jeux-vidéos existant provenant d'une API extérieure à SecondLife ou d'une BDD locale (données en dur)
    /// </summary>
    public class Game
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        [Range(0,100)]
        public float Score { get; set; }

        public string Description { get; set; }

        public string PictureUrl { get; set; }
    }
}