﻿namespace SecondLifeModel.Entities
{
    /// <summary>
    /// Représente un couple Code/Libellé pour indiquer l'état d'une proposition
    /// 
    /// 0 : En attente
    /// 1 : Acceptée
    /// 2 : Refusée
    /// 3 : Annulée
    /// 
    /// </summary>
    public enum ProposalState : int
    {
        Waiting = 0,
        Accepted = 1,
        Refused = 2,
        Canceled = 3
    }
}