﻿using SecondLifeModel.Entities;

namespace SecondLife.Repositories
{
    public interface IProposalRepositories : IRepositorie<Proposal> { }
}