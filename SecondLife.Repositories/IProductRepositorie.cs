﻿using SecondLifeModel.Entities;

namespace SecondLife.Repositories
{
    public interface IProductRepositorie : IRepositorie<Product> { }
}