﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SecondLife.Repositories;
using SecondLife.Service;
using SecondLife.Service.Services;
using SecondLifeModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace SecondLife.Services.Tests
{
    [TestClass]
    public class AuthServiceTests
    {
        private AuthService _service;
        private Mock<IRepositorie<User>> _repo;
        private IConfiguration _config;
        private Mock<IValidator<User>> _validator;
        private Mock<IService<User>> _userService;

        public User getUser() { return new User() { Id = 1, Mail = "anthony@test.com", Username = "anthony", Password = "password", PhoneNumber = "0606060606", StreetNumber = 12, Street = "rue de Paris", City = "STRASBOURG", PostalCode = "67000" }; }

        public AuthServiceTests()
        {
            _repo = new Mock<IRepositorie<User>>();

            _config = new ConfigurationBuilder()
                .AddInMemoryCollection(new Dictionary<string, string> { 
                    { "Jwt:Key", "fakeKeyfakeKeyfakeKeyfakeKeyfakeKey" }, 
                    { "Jwt:Issuer", "fakeIssuerfakeIssuerfakeIssuerfakeIssuer" } 
                })
                .Build();

            _validator = new Mock<IValidator<User>>();
            _userService = new Mock<IService<User>>();
            _service = new AuthService(_repo.Object, _config, _validator.Object, _userService.Object);
        }

        [TestMethod]
        public void CallSignUp_WithUnknownUser_ThenUserIsAddedToDatabase()
        {
            _validator.Setup(x => x.CanAdd(It.IsAny<User>())).Returns(true);

            _userService.Setup(u => u.Add(It.IsAny<User>())).Returns(getUser());

            var signedUpUser = _service.SignUp(getUser());
            
            Assert.AreEqual(getUser().Username, signedUpUser.Username);
        }

        [TestMethod]
        public void CallSignUp_WithKnownUser_ThenUserIsNotAddedToDatabase()
        {
            _validator.Setup(x => x.CanAdd(It.IsAny<User>())).Returns(false);

            var signedUpUser = _service.SignUp(getUser());

            Assert.IsNull(signedUpUser);
        }

        [TestMethod]
        public void CallSignIn_WithUnknownUser_ThenReturnsEmptyString()
        {
            var res = _service.SignIn(new User() { });

            Assert.AreEqual(res, "");
        }

        [TestMethod]
        public void CallSignIn_WithKnownUser_ThenReturnsToken()
        {
            var user = getUser();

            _repo.Setup(x => x.Find(It.IsAny<Expression<Func<User, bool>>>())).Returns(user);

            var res = _service.SignIn(user);

            Assert.AreNotEqual(res, "");
        }

        [TestMethod]
        public void CallWhoAmI_WithBadToken_ThenReturnsNull()
        {
            _repo.Setup(x => x.Find(It.IsAny<Expression<Func<User, bool>>>())).Returns((User)null);

            var res = _service.WhoAmI("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWJqZWN0IjoiSfbdfiBkb2UiLCJhZG1pbiI6dHJ1ZSwiaWF0IjoiMTQ4NTk2ODEwNSJ9.fiSiLFuR4RYuw606Djr2KtQ7y2u-G6OzlHchzklBcd0");

            Assert.IsNull(res);
        }

        [TestMethod]
        public void CallWhoAmI_WithValidToken_ThenReturnsUser()
        {
            var user = getUser();

            _repo.Setup(x => x.Find(It.IsAny<Expression<Func<User, bool>>>())).Returns(user);

            var res = _service.WhoAmI("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWJqZWN0IjoiSfbdfiBkb2UiLCJhZG1pbiI6dHJ1ZSwiaWF0IjoiMTQ4NTk2ODEwNSJ9.fiSiLFuR4RYuw606Djr2KtQ7y2u-G6OzlHchzklBcd0");

            Assert.AreEqual(res, user);
        }

        [TestMethod]
        public void CallPatch_WithUnknownUser_ThenReturnsNull()
        {
            var user = getUser();

            _validator.Setup(x => x.CanEdit(It.IsAny<User>())).Returns(false);
            _userService.Setup(x => x.Get(It.IsAny<int>())).Returns((User)null);

            var failedPatchUser = _service.Patch(998, new JsonPatchDocument<User>());

            Assert.IsNull(failedPatchUser);
        }

        [TestMethod]
        public void CallPatch_WithBadDocumentParam_ThenReturnsUnchangedUser()
        {
            var user = getUser();

            _validator.Setup(x => x.CanEdit(It.IsAny<User>())).Returns(true);
            _userService.Setup(x => x.Get(It.IsAny<int>())).Returns(user);
            _userService.Setup(x => x.Patch(It.IsAny<int>(), It.IsAny<JsonPatchDocument<User>>())).Returns(user);

            var failedPatchUser = _service.Patch(1, new JsonPatchDocument<User>());

            Assert.AreEqual(user, failedPatchUser);
        }

        [TestMethod]
        public void CallPatch_WithKnownUser_ThenReturnsUserChanged()
        {
            var user = getUser();

            var modifiedUser = new User() { Id = 1, Mail = "anthony@test.com", Username = "usernameChanged", Password = "passwordChanged", PhoneNumber = "0606060606", StreetNumber = 12, Street = "rue de Paris", City = "STRASBOURG", PostalCode = "67000" };

            _validator.Setup(x => x.CanEdit(It.IsAny<User>())).Returns(true);
            _userService.Setup(x => x.Get(1)).Returns(user);
            _userService.Setup(x => x.Patch(It.IsAny<int>(), It.IsAny<JsonPatchDocument<User>>())).Returns(modifiedUser);

            var patchedUser = _service.Patch(1, new JsonPatchDocument<User>());

            Assert.AreEqual(patchedUser, modifiedUser);
        }

        [TestMethod]
        public void CallFind_WithWrongUsername_ThenReturnsNull()
        {
            var user = getUser();

            var modifiedUser = new User() { Id = 1, Mail = "anthony@test.com", Username = "usernameChanged", Password = "passwordChanged", PhoneNumber = "0606060606", StreetNumber = 12, Street = "rue de Paris", City = "STRASBOURG", PostalCode = "67000" };

            _validator.Setup(x => x.CanEdit(It.IsAny<User>())).Returns(true);
            _userService.Setup(x => x.Get(1)).Returns(user);
            _userService.Setup(x => x.Patch(It.IsAny<int>(), It.IsAny<JsonPatchDocument<User>>())).Returns(modifiedUser);

            var patchedUser = _service.Patch(1, new JsonPatchDocument<User>());

            Assert.AreEqual(patchedUser, modifiedUser);
        }

        [TestMethod]
        public void CallFind_WithGoodUsername_ThenReturnsUser()
        {
            var user = getUser();

            var modifiedUser = new User() { Id = 1, Mail = "anthony@test.com", Username = "usernameChanged", Password = "passwordChanged", PhoneNumber = "0606060606", StreetNumber = 12, Street = "rue de Paris", City = "STRASBOURG", PostalCode = "67000" };

            _validator.Setup(x => x.CanEdit(It.IsAny<User>())).Returns(true);
            _userService.Setup(x => x.Get(1)).Returns(user);
            _userService.Setup(x => x.Patch(It.IsAny<int>(), It.IsAny<JsonPatchDocument<User>>())).Returns(modifiedUser);

            var patchedUser = _service.Patch(1, new JsonPatchDocument<User>());

            Assert.AreEqual(patchedUser, modifiedUser);
        }

        [TestMethod]
        public void CallCheckCreditentialUser_WithUnknownUser_ThenReturnsNull()
        {
            _repo.Setup(x => x.Find(It.IsAny<Expression<Func<User, bool>>>())).Returns((User)null);

            var user = _service.CheckCreditentialUser(new User() { });

            Assert.IsNull(user);
        }

        [TestMethod]
        public void CallCheckCreditentialUser_WithExistingUser_ThenReturnsEmptyPassword()
        {
            var user = getUser();

            _repo.Setup(x => x.Find(It.IsAny<Expression<Func<User, bool>>>())).Returns(user);

            var res = _service.CheckCreditentialUser(user);

            Assert.IsNull(res.Password);
        }
    }
}
