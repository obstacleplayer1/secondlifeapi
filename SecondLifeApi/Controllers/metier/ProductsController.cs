﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SecondLife.Service.Services.IServices;
using SecondLifeModel.Entities;

namespace SecondLifeApi.Controllers.metier
{
    [ApiController, Route("api/[controller]"), Authorize]
    public class ProductsController : ControllerBase
    {
        private IProductService _service;
        private IAuthService _authService;

        public ProductsController(IProductService service, IAuthService authService)
        {
            _service = service;
            _authService = authService;
        }

        [HttpGet("{id}")]
        public IActionResult GetProduct(int id)
        {
            Product product = _service.GetProduct(id);

            if (product != null)
                return Ok(product);

            return NotFound();
        }
    }
}