using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SecondLifeModel.Entities;
using Microsoft.AspNetCore.JsonPatch;
using SecondLife.Service.Services.IServices;

namespace SecondLifeApi.Controllers
{
    [ApiController, Route("api/[controller]"), Authorize]
    public class AuthController : ControllerBase
    {
        private IAuthService _service;

        public AuthController(IAuthService service)
        {
            _service = service;
        }

        [HttpPost("signup")]
        [AllowAnonymous]
        public IActionResult SignUp([FromBody] User login)
        {
            var user = _service.SignUp(login);

            if (user != null)
                return Created("Account created", user);

            return Conflict("This Username/Mail already exists");
        }

        [HttpPost("signin")]
        [AllowAnonymous]
        public IActionResult SignIn([FromBody] User login)
        {
            var token = _service.SignIn(login);

            if (token != "")
                return Ok(new { token = token });

            return Forbid();
        }

        [HttpGet("whoami")]
        public IActionResult WhoAmI()
        {
            var u = _service.WhoAmI(_service.GetTokenFromRequest(Request));

            if (u != null)
                return Ok(u);

            return Unauthorized();
        }

        [HttpPut("account")]
        public IActionResult Update([FromBody] User userToUpdate)
        {
            var u = _service.WhoAmI(_service.GetTokenFromRequest(Request));

            if (u != null)
            {
                if (u.Id != userToUpdate.Id)
                    return Unauthorized("You can't update an other user than yourself");

                _service.Update(userToUpdate);
                return Ok();
            }

            return Unauthorized();
        }

        [HttpPatch("account/{id}")]
        public IActionResult Patch(int id, [FromBody] JsonPatchDocument<User> document)
        {
            var u = _service.WhoAmI(_service.GetTokenFromRequest(Request));

            if (u != null)
            {
                if (id != u.Id)
                    return Unauthorized("You can't modify an other user than yourself");

                var userPatched = _service.Patch(id, document);

                if (userPatched != null)
                    return Ok(userPatched);

                return Conflict("Cannot modify the user with id: " + id);
            }

            return Unauthorized();
        }
    }
}