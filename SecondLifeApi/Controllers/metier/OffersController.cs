﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SecondLife.Service.Services.IServices;
using SecondLifeModel.Entities;
using System.Linq;

namespace SecondLifeApi.Controllers.metier
{
    [ApiController, Route("api/[controller]"), Authorize]
    public class OffersController : ControllerBase
    {
        private IOfferService _service;
        private IProposalService _proposalService;
        private IAuthService _authService;

        public OffersController(IOfferService service, IProposalService proposalService, IAuthService authService)
        {
            _service = service;
            _proposalService = proposalService;
            _authService = authService;
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteMyOffer(int id)
        {
            var u = _authService.WhoAmI(_authService.GetTokenFromRequest(Request));

            if (u == null)
                return Unauthorized();

            var deleted = _service.DeleteMyOffer(u, id);

            if (deleted)
                return NoContent();

            return Conflict("The offer you've tried to delete doesn't exist");
        }

        [HttpPost]
        public IActionResult CreateAnOffer(Offer offer)
        {
            var u = _authService.WhoAmI(_authService.GetTokenFromRequest(Request));
            
            if (u == null)
                return Unauthorized();

            var o = _service.CreateAnOffer(offer, u);

            if (o != null)
                return Ok(o);
            
            return Conflict("Cannot create this offer");
        }

        [HttpGet]
        public IActionResult GetAllOffersWithoutBuyer()
        {
            var list = _service.GetAllOffersWithoutBuyer();

            if (list.Count() > 0)
                return Ok(list);

            return NoContent();
        }

        [HttpGet("{id}")]
        public IActionResult GetOfferById(int id)
        {
            // TODO 
            var o = _service.GetOfferById(id);

            if (o != null)
                return Ok(o);

            return Forbid();
        }

        // Liste des offres mises en vente par l'utilisateur
        [HttpGet("beingSold")]
        public IActionResult GetMyOffersBeingSold()
        {
            var u = _authService.WhoAmI(_authService.GetTokenFromRequest(Request));

            if (u == null)
                return Unauthorized();

            var list = _service.GetMyOffersBeingSold(u);

            if (list != null && list.Count() > 0)
                return Ok(list);

            return NoContent();
        }

        // Liste des offres sur lesquelles l'utilisateur a fait une Proposition
        [HttpGet("beingPurchased")]
        public IActionResult GetMyOffersBeingPurchased()
        {
            var u = _authService.WhoAmI(_authService.GetTokenFromRequest(Request));

            if (u == null)
                return Unauthorized();

            var list = _service.GetMyOffersBeingPurchased(u);

            if (list != null && list.Count() > 0)
                return Ok(list);

            return NoContent();
        }

        [HttpPost("proposal")]
        public IActionResult MakeAProposal(Proposal proposal)
        {
            var u = _authService.WhoAmI(_authService.GetTokenFromRequest(Request));

            if (u == null)
                return Unauthorized();

            var p = _service.MakeAProposal(u, proposal);

            if (p != null)
                return Ok(p);

            return Conflict("You can't make a proposal if you own the offer or if being the buyer you already have an active proposal for this offer");
        }

        [HttpGet("{id}/proposals")]
        public IActionResult GetOffersProposals(int id)
        {
            var o = _service.GetOfferById(id);
            // TODO validator
            if (o == null)
                return NotFound();

            var proposalsList = _proposalService.GetOffersProposals(o);

            if (proposalsList != null)
                return Ok(proposalsList);

            return Conflict("Cannot create this proposal");
        }

        [HttpGet("{id}/activeProposal")]
        public IActionResult CheckIfActiveProposal(int id)
        {
            var u = _authService.WhoAmI(_authService.GetTokenFromRequest(Request));

            if (u == null)
                return Unauthorized();

            var offer = _service.GetOfferById(id);

            if (offer == null)
                return NotFound();

            bool res = _service.CheckIfActiveProposal(u, offer);

            return Ok(new { activeProposal = res });
        }

        [HttpGet("proposals/{id}/decline")]
        public IActionResult DeclineProposal(int id)
        {
            var u = _authService.WhoAmI(_authService.GetTokenFromRequest(Request));

            if (u == null)
                return Unauthorized();

            var p = _service.DeclineProposal(u, id);

            if (p != null)
                return Ok(p);

            return Conflict("You don't have the permission to refuse this proposal");
        }

        [HttpPost("proposals/{id}/accept")]
        public IActionResult AcceptProposal(int id, [FromBody] string message)
        {
            var u = _authService.WhoAmI(_authService.GetTokenFromRequest(Request));

            if (u == null)
                return Unauthorized();

            var p = _service.AcceptProposal(u, id, message);

            if (p != null)
            {
                _service.AddBuyerToAnOffer(p.Offer, p.RequestingUser);
                return Ok(p);
            }

            return Forbid();
        }

        [HttpPost("{id}/addOpinion")]
        public IActionResult AddAnOpinion(Opinion opinion, int id)
        {
            var u = _authService.WhoAmI(_authService.GetTokenFromRequest(Request));

            if (u == null)
                return Unauthorized();

            var offer = _service.GetOfferById(id);

            if (offer == null)
                return NotFound();

            //On vérifie s'il n'existe pas déjà un opinion pour cette offre
            if (_service.CheckIfOpinionExists(offer))
                return Conflict("This offer already has an opinion");

            var opinionCreated = _service.AddAnOpinion(opinion, offer, u);

            if (opinionCreated != null)
                return Ok(opinionCreated);

            return Forbid();
        }
    }
}