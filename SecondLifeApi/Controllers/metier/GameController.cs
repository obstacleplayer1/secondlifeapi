﻿using Microsoft.AspNetCore.Mvc;
using SecondLife.Service.Services.metier;
using SecondLifeModel.Entities;
using System.Collections.Generic;

namespace SecondLifeApi.Controllers.metier
{
    [ApiController, Route("api/[controller]")]
    public class GameController : ControllerBase
    {
        private GameService _game;

        public GameController(GameService game)
        {
            _game = game;
        }

        [HttpGet]
        public ActionResult<List<Game>> List()
        {
            return Ok(_game.GetAllGame());
        }
    }
}
