﻿using System.Collections.Generic;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using SecondLife.Service;
using SecondLifeModel.Entities;

namespace SecondLifeApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GameCrudController : ControllerBase
    {
        private IService<Game> _game;

        public GameCrudController(IService<Game> game)
        {
            _game = game;
        }

        [HttpGet("{id}")]
        public ActionResult<Game> Get(int id)
        {
            if (id != 0)
                return _game.Get(id);

            return NoContent();
        }

        [HttpGet]
        public ActionResult<List<Game>> List()
        {
            var res = _game.All();

            if (res.Count == 0)
                return NoContent();

            return res;
        }

        [HttpPost]
        public IActionResult Create(Game game)
        {
            var res = _game.Add(game);

            if (res == null)
                return BadRequest();

            return Ok(res);
        }

        [HttpPatch("{id}")]
        public ActionResult<Game> Patch(int id, [FromBody] JsonPatchDocument<Game> document) // [FromBody] forcer l’API Web à lire un type simple à partir du corps de la requête
        {
            var updatedGame = _game.Patch(id, document);

            return Ok(updatedGame);
        }

        [HttpPut]
        public ActionResult<Game> Update(Game game)
        {
            _game.Update(game);

            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            if (id != 0)
                if (_game.Get(id) != null)
                    if (_game.Delete(id))
                        return Ok();

            return NotFound();
        }
    }
}
