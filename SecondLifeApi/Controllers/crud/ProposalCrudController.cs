﻿using System.Collections.Generic;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using SecondLife.Service;
using SecondLifeModel.Entities;

namespace SecondLifeApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProposalCrudController : ControllerBase
    {
        private IService<Proposal> _proposal;

        public ProposalCrudController(IService<Proposal> proposal)
        {
            _proposal = proposal;
        }

        [HttpGet("{id}")]
        public ActionResult<Proposal> Get(int id)
        {
            if (id != 0)
                return _proposal.Get(id);

            return NoContent();
        }

        [HttpGet]
        public ActionResult<List<Proposal>> List()
        {
            var res = _proposal.All();

            if (res.Count == 0)
                return NoContent();

            return res;
        }

        [HttpPost]
        public ActionResult<Proposal> Create(Proposal proposal)
        {
            Proposal res = null;

            if (proposal.Offer != null &&
                proposal.ProposedPrice > 0 &&
                proposal.ProposedProducts != null &&
                proposal.RequestingUser != null)
                res = _proposal.Add(proposal);

            if (res == null)
                return BadRequest();

            return res;
        }

        [HttpPatch("{id}")]
        public ActionResult<Proposal> Patch(int id, [FromBody] JsonPatchDocument<Proposal> document) // [FromBody] forcer l’API Web à lire un type simple à partir du corps de la requête
        {
            var updatedProposal = _proposal.Patch(id, document);

            return Ok(updatedProposal);
        }

        [HttpPut]
        public ActionResult<Proposal> Update(Proposal proposal)
        {
            _proposal.Update(proposal);

            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            if (id != 0)
                if (_proposal.Get(id) != null)
                    if (_proposal.Delete(id))
                        return Ok();

            return NotFound();
        }
    }
}