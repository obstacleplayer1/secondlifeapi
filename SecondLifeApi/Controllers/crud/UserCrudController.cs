using System.Collections.Generic;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using SecondLife.Service;
using SecondLifeModel.Entities;

namespace SecondLifeApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserCrudController : ControllerBase
    {
        private IService<User> _user;

        public UserCrudController(IService<User> user)
        {
            _user = user;
        }

        [HttpGet("{id}")]
        public ActionResult<User> Get(int id)
        {
            if (id != 0)
                if (_user.Get(id) != null)
                    return _user.Get(id);

            return NoContent();
        }

        [HttpGet]
        public ActionResult<List<User>> List()
        {
            var res = _user.All();

            if (res.Count == 0)
                return NoContent();

            return res;
        }

        [HttpPost]
        public ActionResult<User> Create(User user)
        {
            User res = null;

            if (user.Mail != null &&
                user.Username != null &&
                user.Password != null &&
                user.PhoneNumber != null &&
                user.StreetNumber > 0 &&
                user.Street != null &&
                user.City != null &&
                user.PostalCode != null)
                res = _user.Add(user);

            if (res == null)
                return BadRequest();

            return res;
        }

        [HttpPatch("{id}")]
        public ActionResult<User> Patch(int id, [FromBody] JsonPatchDocument<User> document) // [FromBody] forcer l’API Web à lire un type simple à partir du corps de la requête
        {
            var updatedUser = _user.Patch(id, document);

            return Ok(updatedUser);
        }

        [HttpPut]
        public ActionResult<User> Update(User user)
        {
            _user.Update(user);

            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            if (id != 0)
                if (_user.Get(id) != null)
                    if (_user.Delete(id))
                        return Ok();

            return NotFound();
        }
    }
}