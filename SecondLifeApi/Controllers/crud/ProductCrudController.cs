using SecondLifeModel.Entities;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using SecondLife.Service;

namespace SecondLifeApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductCrudController : ControllerBase
    {
        private IService<Product> _product;

        public ProductCrudController(IService<Product> product)
        {
            _product = product;
        }

        [HttpGet("{id}")]
        public ActionResult<Product> Get(int id)
        {
            if (id != 0)
                return _product.Get(id);

            return NoContent();
        }

        [HttpGet]
        public ActionResult<List<Product>> List()
        {
            var res = _product.All();

            if (res.Count == 0)
                return NoContent();

            return res;
        }

        [HttpPost]
        public ActionResult<Product> Create(Product product)
        {
            Product res = null;

            if (product.Name != null && 
                product.Description != null && 
                product.PictureUrl != null && 
                product.Game != null)
                res = _product.Add(product);

            if (res == null)
                return BadRequest();

            return res;
        }

        [HttpPatch("{id}")]
        public ActionResult<Product> Patch(int id, [FromBody] JsonPatchDocument<Product> document) // [FromBody] forcer l’API Web à lire un type simple à partir du corps de la requête
        {
            var updatedProduct = _product.Patch(id, document);

            return Ok(updatedProduct);
        }

        [HttpPut]
        public ActionResult<Product> Update(Product product)
        {
            _product.Update(product);

            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            if (id != 0)
                if (_product.Get(id) != null)
                    if (_product.Delete(id))
                        return Ok();

            return NotFound();
        }
    }
}