﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SecondLife.Service.Services.IServices;
using SecondLifeModel.Entities;

namespace SecondLifeApi.Controllers.Tests
{
    [TestClass]
    public class AuthControllerTests
    {
        private AuthController _controller;
        private Mock<HttpRequest> _mockRequest;
        private Mock<IAuthService> _authServiceMock;

        public AuthControllerTests()
        {
            _mockRequest = new Mock<HttpRequest>();
            _authServiceMock = new Mock<IAuthService>();
            _controller = new AuthController(_authServiceMock.Object);
        }

        public User getUser() { return new User() { Mail = "anthony@test.com", Username = "anthony", Password = "password", PhoneNumber = "0606060606", StreetNumber = 12, Street = "rue de Paris", City = "STRASBOURG", PostalCode = "67000" }; }

        [TestMethod]
        public void CallSignUpWithUnknownUser_ThenReturnsForbid()
        {
            _authServiceMock.Setup(x => x.SignUp(It.IsAny<User>())).Returns((User)null);

            var whoAmI = _controller.SignUp((User)null);

            Assert.IsInstanceOfType(whoAmI, typeof(ConflictObjectResult));
        }

        [TestMethod]
        public void CallSignUpWithKnownUser_ThenReturnsOk()
        {
            _authServiceMock.Setup(x => x.SignUp(It.IsAny<User>())).Returns(getUser());

            var whoAmI = _controller.SignUp(getUser());

            Assert.IsInstanceOfType(whoAmI, typeof(CreatedResult));
        }

        [TestMethod]
        public void CallSignInWithUnknownUser_ThenReturnsForbid()
        {
            _authServiceMock.Setup(x => x.SignIn(It.IsAny<User>())).Returns("");

            var whoAmI = _controller.SignIn(getUser());

            Assert.IsInstanceOfType(whoAmI, typeof(ForbidResult));
        }

        [TestMethod]
        public void CallSignInWithKnownUser_ThenReturnsOk()
        {
            _authServiceMock.Setup(x => x.SignIn(It.IsAny<User>())).Returns("bearer_token");

            var whoAmI = _controller.SignIn(getUser());

            Assert.IsInstanceOfType(whoAmI, typeof(OkObjectResult));
        }

        [TestMethod]
        public void CallWhoAmIWithUnknownUser_ThenReturnsForbid()
        {
            _authServiceMock.Setup(x => x.WhoAmI(It.IsAny<string>())).Returns((User)null);

            var whoAmI = _controller.WhoAmI();

            Assert.IsInstanceOfType(whoAmI, typeof(UnauthorizedResult));
        }

        [TestMethod]
        public void CallWhoAmIWithKnownUser_ThenWhoAmIReturnsOk()
        {
            _authServiceMock.Setup(m => m.WhoAmI(It.IsAny<string>())).Returns(getUser());

            var whoAmI = _controller.WhoAmI();

            Assert.IsInstanceOfType(whoAmI, typeof(OkObjectResult));
        }

        [TestMethod]
        public void CallPatchWithNullUser_ThenReturnsUnauthorized()
        {
            _authServiceMock.Setup(x => x.WhoAmI(It.IsAny<string>())).Returns((User)null);

            var whoAmI = _controller.Patch(0, new JsonPatchDocument<User>());

            Assert.IsInstanceOfType(whoAmI, typeof(UnauthorizedResult));
        }

        [TestMethod]
        public void CallPatchWithUnknownUser_ThenReturnsConflict()
        {
            _authServiceMock.Setup(x => x.WhoAmI(It.IsAny<string>())).Returns(getUser());
            _authServiceMock.Setup(x => x.Patch(It.IsAny<int>(), It.IsAny<JsonPatchDocument<User>>())).Returns((User)null);

            var whoAmI = _controller.Patch(getUser().Id, new JsonPatchDocument<User>());

            Assert.IsInstanceOfType(whoAmI, typeof(ConflictObjectResult));
        }

        [TestMethod]
        public void CallPatchWithKnownUser_ThenWhoAmIReturnsOk()
        {
            _authServiceMock.Setup(x => x.WhoAmI(It.IsAny<string>())).Returns(getUser());
            _authServiceMock.Setup(x => x.Patch(It.IsAny<int>(), It.IsAny<JsonPatchDocument<User>>())).Returns(getUser());

            var whoAmI = _controller.Patch(getUser().Id, new JsonPatchDocument<User>());

            Assert.IsInstanceOfType(whoAmI, typeof(OkObjectResult));
        }
    }
}