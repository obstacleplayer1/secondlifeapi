using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SecondLife.Service;
using SecondLifeModel.Entities;

namespace SecondLifeApi.Controllers.Tests
{
    [TestClass]
    public class OpinionControllerTests
    {
        private OpinionCrudController _controller;
        private UserCrudController _userController;
        private Mock<IService<Opinion>> _opinionServiceMock;
        private Mock<IService<User>> _userServiceMock;

        public OpinionControllerTests()
        {
            _opinionServiceMock = new Mock<IService<Opinion>>();
            _userServiceMock = new Mock<IService<User>>();
            _controller = new OpinionCrudController(_opinionServiceMock.Object);
            _userController = new UserCrudController(_userServiceMock.Object);
        }

        //Un GET Opinion avec un id � 0 doit renvoyer une erreur
        [TestMethod]
        public void Get_With0_ThenGetFromRepoIsNotCalled()
        {
            _controller.Get(0);
            _opinionServiceMock.Verify(x => x.Get(0), Times.Never());
        }

        //Un GET Opinion avec un id � 1 doit renvoyer un r�sultat
        [TestMethod]
        public void Get_With1_ThenGetFromRepoIsCalledWith1()
        {
            _controller.Get(1);
            _opinionServiceMock.Verify(x => x.Get(1), Times.Once());
        }

        //La cr�ation d'un Opinion sans param�tres doit �chouer
        [TestMethod]
        public void Add_WithNoParameters_ThenAddIsNotCalled()
        {
            _controller.Create(new Opinion());
            _opinionServiceMock.Verify(x => x.Add(It.IsAny<Opinion>()), Times.Never());
        }

        //La cr�ation d'un Opinion avec des param�tres manquants doit �chouer
        [TestMethod]
        public void Add_WithMissingParameters_ThenAddIsNotCalled()
        {
            _controller.Create(new Opinion()
            {
                RequestingUser = _userController.Get(1).Value,
                TargetedUser = _userController.Get(2).Value,
            });

            _opinionServiceMock.Verify(x => x.Add(It.IsAny<Opinion>()), Times.Never());
        }

        //La cr�ation d'un Opinion avec tous les param�tres n�cessaires doit r�ussir
        [TestMethod]
        public void Add_WithAllParameters_ThenAddIsCalled()
        {
            _controller.Create(new Opinion()
            {
                RequestingUser = new User() { Id = 1 },
                TargetedUser = new User() { Id = 2 },
                Comment = "Commentaire",
                Note = 4
            });

            _opinionServiceMock.Verify(x => x.Add(It.IsAny<Opinion>()), Times.Once());
        }

        //La suppression d'un enregistrement en base de donn�es avec un ID inexistant doit �chouer
        [TestMethod]
        public void DeleteWithUnknownId_ThenDeleteIsNotOk()
        {
            _opinionServiceMock.Setup(x => x.Delete(It.IsAny<int>())).Returns(false);

            var deletedOpinion = _controller.Delete(55);

            Assert.IsInstanceOfType(deletedOpinion, typeof(NotFoundResult));
        }

        //La suppression d'un enregistrement en base de donn�es avec un ID existant doit r�ussir
        [TestMethod]
        public void DeleteWithExistingId_ThenDeleteIsOk()
        {
            _opinionServiceMock.Setup(x => x.Get(It.IsAny<int>())).Returns(new Opinion());
            _opinionServiceMock.Setup(x => x.Delete(It.IsAny<int>())).Returns(true);

            var deletedOpinion = _controller.Delete(1);

            Assert.IsInstanceOfType(deletedOpinion, typeof(OkResult));
        }
    }
}