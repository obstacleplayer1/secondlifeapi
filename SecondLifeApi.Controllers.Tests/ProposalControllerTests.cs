using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SecondLife.Service;
using SecondLifeModel.Entities;
using System.Collections.Generic;

namespace SecondLifeApi.Controllers.Tests
{
    [TestClass]
    public class ProposalControllerTests
    {
        private ProposalCrudController _controller;
        private OfferCrudController _offerController;
        private UserCrudController _userController;
        private ProductCrudController _productController;
        private Mock<IService<Proposal>> _proposalServiceMock;
        private Mock<IService<Offer>> _offerServiceMock;
        private Mock<IService<User>> _userServiceMock;
        private Mock<IService<Product>> _productServiceMock;

        public ProposalControllerTests()
        {
            _proposalServiceMock = new Mock<IService<Proposal>>();
            _offerServiceMock = new Mock<IService<Offer>>();
            _userServiceMock = new Mock<IService<User>>();
            _productServiceMock = new Mock<IService<Product>>();
            _controller = new ProposalCrudController(_proposalServiceMock.Object);
            _offerController = new OfferCrudController(_offerServiceMock.Object);
            _userController = new UserCrudController(_userServiceMock.Object);
            _productController = new ProductCrudController(_productServiceMock.Object);
        }

        //Un GET Proposal avec un id � 0 doit renvoyer une erreur
        [TestMethod]
        public void Get_With0_ThenGetFromRepoIsNotCalled()
        {
            _controller.Get(0);
            _proposalServiceMock.Verify(x => x.Get(0), Times.Never());
        }

        //Un GET Proposal avec un id � 1 doit renvoyer un r�sultat
        [TestMethod]
        public void Get_With1_ThenGetFromRepoIsCalledWith1()
        {
            _controller.Get(1);
            _proposalServiceMock.Verify(x => x.Get(1), Times.Once());
        }

        //La cr�ation d'un Proposal sans param�tres doit �chouer
        [TestMethod]
        public void Add_WithNoParameters_ThenAddIsNotCalled()
        {
            _controller.Create(new Proposal());
            _proposalServiceMock.Verify(x => x.Add(It.IsAny<Proposal>()), Times.Never());
        }

        //La cr�ation d'un Proposal avec des param�tres manquants doit �chouer
        [TestMethod]
        public void Add_WithMissingParameters_ThenAddIsNotCalled()
        {
            _controller.Create(new Proposal()
            {
                Offer = _offerController.Get(1).Value
            });

            _proposalServiceMock.Verify(x => x.Add(It.IsAny<Proposal>()), Times.Never());
        }

        //La cr�ation d'un Proposal avec tous les param�tres n�cessaires doit r�ussir
        [TestMethod]
        public void Add_WithAllParameters_ThenAddIsCalled()
        {
            _controller.Create(new Proposal()
            {
                Offer = new Offer() { Id = 1 },
                RequestingUser = new User() { Id = 1 },
                ProposedProducts = new List<Product>() { _productController.Get(1).Value },
                ProposedPrice = 15
            });

            _proposalServiceMock.Verify(x => x.Add(It.IsAny<Proposal>()), Times.Once());
        }

        //La suppression d'un enregistrement en base de donn�es avec un ID inexistant doit �chouer
        [TestMethod]
        public void DeleteWithUnknownId_ThenDeleteIsNotOk()
        {
            _proposalServiceMock.Setup(x => x.Delete(It.IsAny<int>())).Returns(false);

            var deletedProposal = _controller.Delete(55);

            Assert.IsInstanceOfType(deletedProposal, typeof(NotFoundResult));
        }

        //La suppression d'un enregistrement en base de donn�es avec un ID existant doit r�ussir
        [TestMethod]
        public void DeleteWithExistingId_ThenDeleteIsOk()
        {
            _proposalServiceMock.Setup(x => x.Get(It.IsAny<int>())).Returns(new Proposal());
            _proposalServiceMock.Setup(x => x.Delete(It.IsAny<int>())).Returns(true);

            var deletedProposal = _controller.Delete(1);

            Assert.IsInstanceOfType(deletedProposal, typeof(OkResult));
        }
    }
}