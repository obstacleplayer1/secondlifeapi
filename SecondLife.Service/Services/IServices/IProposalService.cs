﻿using SecondLifeModel.Entities;
using System.Collections.Generic;

namespace SecondLife.Service.Services.IServices
{
    public interface IProposalService
    {
        public Proposal GetById(int id);
        public Proposal Create(Proposal proposal);
        public List<Proposal> GetAllProposals();
        public List<Proposal> GetAllUserProposals(User u);
        public List<Proposal> GetOffersProposals(Offer o);
        public Proposal DeclineProposal(Proposal proposal);
        public Proposal AcceptProposal(List<Proposal> proposalList, int incomingProposalId, string message);
    }
}
