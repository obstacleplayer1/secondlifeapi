﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using SecondLifeModel.Entities;

namespace SecondLife.Service.Services.IServices
{
    public interface IAuthService
    {
        public User SignUp(User user);
        public User Patch(int id, JsonPatchDocument<User> document);
        public string SignIn(User user);
        public User WhoAmI(string bearer_token);
        public void Update(User user);
        public User CheckCreditentialUser(User user);
        public User Find(string username);
        public User Find(string username, string password);
        public string GetTokenFromRequest(HttpRequest httpRequest);
    }
}