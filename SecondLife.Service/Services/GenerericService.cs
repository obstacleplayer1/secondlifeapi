using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.AspNetCore.JsonPatch;
using SecondLife.Repositories;
using SecondLife.Service.Services.Validator;

namespace SecondLife.Service
{
    public class GenerericService<T> : IService<T> where T : class
    {
        protected IRepositorie<T> _repositorie;

        public GenerericService(IRepositorie<T> repositorie)
        {
            _repositorie = repositorie;
        }

        public List<T> All(Expression<Func<T, bool>> condition = null)
        {
            return _repositorie.All(condition);
        }

        public T Get(int serviceGet)
        {
            return _repositorie.Get(serviceGet);
        }

        public T One()
        {
            return _repositorie.One();
        }

        public T Add(T serviceAdd)
        {
            return _repositorie.Add(serviceAdd);
        }

        public void Update(T serviceUpdate)
        {
            _repositorie.Update(serviceUpdate);
        }

        public bool Delete(int serviceDelete)
        {
            return _repositorie.Delete(serviceDelete);
        }

        public bool Exists(T serviceExists)
        {
            return _repositorie.Exists(serviceExists);
        }

        public T Patch(int id, JsonPatchDocument<T> document)
        {
            var updated = Get(id);
            document.ApplyTo(updated);
            Update(updated);
            return updated;
        }

        public T Find(Expression<Func<T, bool>> condition)
        {
            return _repositorie.Find(condition);
        }

    }
}