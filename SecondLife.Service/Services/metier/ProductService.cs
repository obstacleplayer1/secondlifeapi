﻿using SecondLife.Service.Services.IServices;
using SecondLifeModel.Entities;

namespace SecondLife.Service.Services.metier
{
    public class ProductService : IProductService
    {
        private IService<Product> _service;

        public ProductService(IService<Product> service)
        {
            _service = service;
        }

        public Product GetProduct(int id)
        {
            return _service.Get(id);
        }
    }
}