﻿using SecondLife.Service.Services.IServices;
using SecondLife.Service.Services.Validator;
using SecondLifeModel.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SecondLife.Service.Services.metier
{
    public class ProposalService : IProposalService
    {
        private IService<Proposal> _service;
        private IValidator<Proposal> _proposalValidator;

        public ProposalService(IService<Proposal> service, IValidator<Proposal> proposalValidator)
        {
            _service = service;
            _proposalValidator = proposalValidator;
        }

        public Proposal Create(Proposal proposal)
        {
            if (_proposalValidator.CanAdd(proposal))
                return _service.Add(proposal);
          
            return null;
        }

        public Proposal GetById(int id)
        {
            return _service.Get(id);
        }

        public List<Proposal> GetAllProposals()
        {
            return _service.All();
        }

        public List<Proposal> GetOffersProposals(Offer o)
        {
            return _service.All(p => p.Offer.Id == o.Id);
        }

        public Proposal DeclineProposal(Proposal proposal)
        {
            proposal.ProposalState = ProposalState.Refused;

            _service.Update(proposal);
            
            return proposal;
        }

        public Proposal AcceptProposal(List<Proposal> listProposals, int incomingProposalId, string message)
        {
            Proposal proposalToAccept = null;

            foreach (var proposal in listProposals)
            {
                //Toute proposition *encore active* qui ne correspond pas à celle passée en paramètre se verra refusée
                if (proposal.Id != incomingProposalId && proposal.ProposalState == ProposalState.Waiting)
                {
                    proposal.ProposalState = ProposalState.Refused;
                    proposal.SellerComment = "Un autre acheteur a remporté l'offre";
                    _service.Update(proposal);
                }
                else if (proposal.Id == incomingProposalId && proposal.ProposalState == ProposalState.Waiting)
                {
                    proposal.ProposalState = ProposalState.Accepted;

                    if (!String.IsNullOrEmpty(message))
                        proposal.SellerComment = message;

                    _service.Update(proposal);

                    proposalToAccept = proposal;
                }
            }

            return proposalToAccept;
        }

        public List<Proposal> GetAllUserProposals(User u)
        {
            return _service.All(p => p.RequestingUser.Id == u.Id).ToList();
        }
    }
}