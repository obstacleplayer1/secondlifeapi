﻿using SecondLife.Repositories;
using SecondLife.Service.Services.IServices;
using SecondLifeModel.Entities;
using System;
using System.Text;
using Microsoft.Extensions.Configuration;
using System.Linq;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Http;

namespace SecondLife.Service.Services
{
    public class AuthService : IAuthService
    {
        private IService<User> _service;
        private IRepositorie<User> _repo;
        private IConfiguration _configuration;
        private IValidator<User> _validator;

        public AuthService(IRepositorie<User> repo, IConfiguration config, IValidator<User> validator, IService<User> service)
        {
            _service = service;
            _repo = repo;
            _configuration = config;
            _validator = validator;
        }

        public string GetTokenFromRequest(HttpRequest httpRequest)
        {
            return httpRequest.Headers["Authorization"].ToString().Replace("Bearer ", "");
        }

        public User SignUp(User user)
        {
            if (user != null)
                if (_validator.CanAdd(user))
                {
                    var a = _service.Add(user);
                    return a; 
                }
                    //return _service.Add(user);

            return null;
        }

        public string SignIn(User user)
        {
            var u = CheckCreditentialUser(user);

            if (u != null) {
                var token = GenerateJSONWebToken(u);
                return token;
            }

            return "";
        }

        public User WhoAmI(string bearer_token) 
        {
            var handler = new JwtSecurityTokenHandler();
            var decodeToken = handler.ReadToken(bearer_token) as JwtSecurityToken;

            var username = decodeToken.Payload.ToArray()[0].Value.ToString();

            User user = Find(username);

            if (user != null) {
                //u.Password = "";
                return user;
            }

            return null;
        }

        public void Update(User u)
        {
            if (_validator.CanEdit(_service.Get(u.Id)))
                _service.Update(u);
        }

        public User Patch(int id, JsonPatchDocument<User> document)
        {
            if (_validator.CanEdit(_service.Get(id)))
            {
                var u = _service.Patch(id, document);

                if (u != null)
                    return u;
            }

            return null;
        }

        public User Find(string username, string password)
        {
            return _repo.Find(x => x.Username == username && x.Password == password);
        }

        public User Find(string username)
        {
            return _repo.Find(x => x.Username == username);
        }

        public User CheckCreditentialUser(User user)
        {
            var u = Find(user.Username, user.Password);
            
            if (u != null) {
                u.Password = null;
                return u;
            }

            return null;
        }

        private string GenerateJSONWebToken(User user)
        {
            var secret = _configuration["Jwt:Key"];
            var secureKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret));
            var credentials = new SigningCredentials(secureKey, SecurityAlgorithms.HmacSha256);

            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.Sub, user.Username),
                new Claim(JwtRegisteredClaimNames.Email, user.Mail),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var token = new JwtSecurityToken(_configuration["Jwt:Issuer"],
                _configuration["Jwt:Issuer"],
                claims,
                expires: DateTime.Now.AddMinutes(120),
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}