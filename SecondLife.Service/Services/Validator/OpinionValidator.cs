﻿using SecondLife.Repositories;
using SecondLifeModel.Entities;

namespace SecondLife.Service.Services.Validator
{
    class OpinionValidator : IValidator<Opinion>
    {
        private IRepositorie<Opinion> _repo;

        public bool CanAdd(Opinion opinion)
        {
            if (opinion.RequestingUser != null && opinion.TargetedUser != null && opinion.Comment != null)
                return true;

            return false;
        }

        public bool CanDelete(int id)
        {
            if (_repo.Exists(_repo.Get(id)))
                return true;

            return false;
        }

        public bool CanEdit(Opinion opinion)
        {
            if (_repo.Exists(_repo.Get(opinion.Id)))
                return true;

            return false;
        }

        public bool CanRead(int id)
        {
            if (_repo.Exists(_repo.Get(id)))
                return true;

            return false;
        }
    }
}